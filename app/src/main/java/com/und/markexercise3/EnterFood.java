package com.und.markexercise3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class EnterFood extends AppCompatActivity {

    private FoodsDataSource datasource;
    EditText name, price, price2;
    private ListView lv;
    private ArrayList<FoodModel> modelArrayList = new ArrayList<>();
    private ArrayList<FoodModel> modelArrayList2 = new ArrayList<>() ;
    private ArrayList<String>modelArrayListStrings;
    private CustomAdapter customAdapter;
    private List<Food> values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        lv = (ListView)findViewById(R.id.lv);
        name = (EditText) findViewById( R.id.name );
        price = (EditText) findViewById( R.id.price );
        price2 = (EditText) findViewById( R.id.price2 );
        datasource = new FoodsDataSource(this);


        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //datasource.deleteTable();

        values = datasource.getAllFoods();
        for (int i = 0; i< values.size(); i++){
            FoodModel model = new FoodModel( values.get(i).getName(), false );
            modelArrayList.add(model);
        }

        customAdapter = new CustomAdapter(this,modelArrayList);
        lv.setAdapter(customAdapter);

        //ArrayAdapter<Food> adapter = new ArrayAdapter<Food>(this, android.R.layout.simple_list_item_1,values);
        //setListAdapter(adapter);

    }

    public void onClick(View view){
        @SuppressWarnings("unchecked")


        //ArrayAdapter<Food> adapter = (ArrayAdapter<Food>) getListAdapter();
        Food food = null;
        food = datasource.createFood(name.getText().toString(),Float.parseFloat(price.getText().toString()));
        System.out.println(food.getName());

        FoodModel model = new FoodModel(food.getName(),false);
        //model.setName(food.getName());
        //model.setSelected(false);
        modelArrayList.add(model);
        customAdapter = new CustomAdapter(EnterFood.this, modelArrayList);
        lv.setAdapter(customAdapter);


    }

    public void onClick2(View view) {

        //modelArrayList2;
        modelArrayListStrings = new ArrayList<>();

        for(int i =0; i< modelArrayList.size();i++){
            if(modelArrayList.get(i).getSelected()){
                modelArrayList2.add(modelArrayList.get(i));
            }
        }

        for(int i =0; i< modelArrayList2.size();i++){
            Toast.makeText(this,"Checkbox "+modelArrayList2.get(i).getName()+" selected! ", Toast.LENGTH_SHORT).show();
            modelArrayListStrings.add(modelArrayList2.get(i).getName());
        }
        String listOfFood = TextUtils.join(":",modelArrayListStrings);
        MealCombo combo = null;
        combo = datasource.createCombo(listOfFood,Float.parseFloat(price2.getText().toString())); ///get combo
        Toast.makeText(this,"Combo created!! ", Toast.LENGTH_SHORT).show();

    }

    public void onClick3(View view) {
        Intent i = new Intent(EnterFood.this,DisplayCombos.class);
        startActivity(i);
    }

    public void onClick4(View view) {
        Intent i = new Intent(EnterFood.this,DisplayFood.class);
        startActivity(i);
    }



    @Override
    protected void onResume(){
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    protected void onPause(){
        datasource.close();
        super.onPause();
    }

//    private ArrayList<FoodModel> getModel(boolean isSelect){
//        ArrayList<FoodModel> list = new ArrayList<>();
//        for(int i =0; i< 4; i++){
//            FoodModel model = new FoodModel();
//            model.setSelected(isSelect);
//            model.setName(foodlist[i]);
//            list.add(model);
//        }
//    }
}
