package com.und.markexercise3;

public class MealCombo {

    private Integer id;
    private String listOfFoodItems;
    private Float price;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getListOfFoodItems() {
        return listOfFoodItems;
    }

    public void setListOfFoodItems(String listOfFoodItems) {
        this.listOfFoodItems = listOfFoodItems;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }



}
