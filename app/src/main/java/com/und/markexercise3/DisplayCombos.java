package com.und.markexercise3;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class DisplayCombos extends AppCompatActivity {

    private FoodsDataSource datasource;
    private ListView lv;
    private List<MealCombo> values;
    private ArrayList<ComboModel> modelArrayList = new ArrayList<>();
    private CustomAdapterRadio customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_combos);


        lv = (ListView)findViewById(R.id.lvCombo);

        datasource = new FoodsDataSource(this);


        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        values = datasource.getAllCombos();

        for (int i = 0; i< values.size(); i++){
            ComboModel model = new ComboModel( values.get(i).getId().toString(), false );
            modelArrayList.add(model);
        }

        customAdapter = new CustomAdapterRadio(this, modelArrayList);
        lv.setAdapter(customAdapter);

    }

    public static void makeTextViewHyperlink( TextView tv ) {
        SpannableStringBuilder ssb = new SpannableStringBuilder( );
        ssb.append( tv.getText( ) );
        ssb.setSpan( new URLSpan("#"), 0, ssb.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
        tv.setText( ssb, TextView.BufferType.SPANNABLE );
    }



}
