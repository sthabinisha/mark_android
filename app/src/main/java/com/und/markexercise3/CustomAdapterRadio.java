package com.und.markexercise3;


import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CustomAdapterRadio extends BaseAdapter {

    private static RadioButton lastChecked = null;
    private static int lastCheckedPos = 0;

    private Context context;
    public static ArrayList<ComboModel> modelArrayList;

    public CustomAdapterRadio(Context context, ArrayList<ComboModel> modelArrayList){
        this.context = context;
        this.modelArrayList = modelArrayList;
    }

    @Override
    public int getViewTypeCount(){
        return 1;
    }
    @Override
    public int getItemViewType(int position){
        return position;
    }

    @Override
    public int getCount(){
        return (modelArrayList ==null)?0: modelArrayList.size();
    }

    @Override
    public Object getItem(int position){
        return modelArrayList.get(position);
    }
    @Override
    public long getItemId(int position){
        return modelArrayList.size();
    }


    @Override
    public View getView(int postion, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null){

            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_radio,null,true);
            holder.radioButton = (RadioButton) convertView.findViewById(R.id.rb);
            holder.tvCombo = (TextView) convertView.findViewById(R.id.combo);



            convertView.setTag(holder);
        }else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        //holder.checkBox.setText("Checkbox "+ postion);
        holder.tvCombo.setText(modelArrayList.get(postion).getId());
        DisplayCombos.makeTextViewHyperlink(holder.tvCombo);

        holder.tvCombo.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  Intent intent = new Intent( context, ComboDetails.class );
                                                  context.startActivity( intent );
                                              }
                                          }

        );

        holder.radioButton.setChecked(modelArrayList.get(postion).getSelected());

        holder.radioButton.setTag(R.integer.btnplusview, convertView);
        holder.radioButton.setTag(postion);
//     holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
////         @Override
//////         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//////             if(holder.checkBox.isChecked()){
//////                 modelArrayList.get(buttonView.getId()).setSelected(true);
//////             }
//////
//////         }
//     });
        holder.radioButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                RadioButton radioButton = (RadioButton)v;

                int ClickedPos = ((Integer)radioButton.getTag()).intValue();

                if(radioButton.isChecked()){
                    if(lastChecked!= null){
                        lastChecked.setChecked(false);
                        //lastChecked.setSelected(false);
                        modelArrayList.get(ClickedPos).setSelected(true);


                    }
                    lastChecked = radioButton;
                    lastCheckedPos = ClickedPos;

                }else{
                    lastChecked = null;
                    modelArrayList.get(ClickedPos).setSelected(true);
                    //textPrice = holder.price.getText().toString();
                }




//                View tempview = (View) holder.radioButton.getTag(R.integer.btnplusview);
//                TextView tv = (TextView) tempview.findViewById(R.id.combo);
//                Integer pos = (Integer) holder.radioButton.getTag();
//                //Toast.makeText(context,"Checkbox "+pos+" clicked!", Toast.LENGTH_SHORT).show();
//
//                if(modelArrayList.get(pos).getSelected()) {
//                    modelArrayList.get(pos).setSelected(false);
//                    Toast.makeText(context,"RadioButton "+pos+" clicked! " +modelArrayList.get(pos).getSelected(), Toast.LENGTH_SHORT).show();
//                }else {
//                    modelArrayList.get(pos).setSelected(true);
//                    Toast.makeText(context,"RadioButton "+pos+" clicked! " +modelArrayList.get(pos).getSelected(), Toast.LENGTH_SHORT).show();
//                }
            }

        });
        return convertView;
    }

    public  int getSelected(){

        return lastCheckedPos;
    }


    private class   ViewHolder{
        protected RadioButton radioButton;
        private TextView tvCombo;
    }



}
