package com.und.markexercise3;

public class Food {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    private String name;
    private Float price;


    @Override
    public String toString(){
        return name;
    }



}
