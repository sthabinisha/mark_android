package com.und.markexercise3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_FOODS = "foods";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PRICE = "price";


    public static final String TABLE_COMBOS = "combos";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LIST_OF_FOOD= "listOfFood";


    private static final String DATABASE_NAME = "applicationdata";
    private static final int DATABASE_VERSION = 1;


    private static final String TABLE_CREATE = "create table "
            + TABLE_FOODS + "( " + COLUMN_NAME + " text not null, "
            + COLUMN_PRICE + " real not null );";

    private static final String TABLE_CREATE2 = "create table "
            + TABLE_COMBOS + "( " + COLUMN_ID +" integer primary key autoincrement, " + COLUMN_LIST_OF_FOOD+ " text not null, "
            + COLUMN_PRICE + " real not null );";


    public MySQLiteHelper(Context context){
        super( context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database){
        database.execSQL( TABLE_CREATE);
        database.execSQL( TABLE_CREATE2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        Log.w( MySQLiteHelper.class.getName(), "Upgrading database from version "
        + oldVersion + " to " + newVersion + ", which will destroy all old data" );
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_FOODS);
        onCreate(db);

        Log.w( MySQLiteHelper.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion + ", which will destroy all old data" );
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_COMBOS);
        onCreate(db);


    }
}
