package com.und.markexercise3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.CompoundButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FoodsDataSource {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_NAME,MySQLiteHelper.COLUMN_PRICE};
    private String[] allColumns2 = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_LIST_OF_FOOD, MySQLiteHelper.COLUMN_PRICE};

    public FoodsDataSource(Context context){

        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException{
        database = dbHelper.getWritableDatabase();

    }

    public void close(){
        dbHelper.close();
    }

    public Food createFood(String food, Float price){

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_NAME, food);
        values.put(MySQLiteHelper.COLUMN_PRICE, price);
        long insertId = database.insert(MySQLiteHelper.TABLE_FOODS, null, values);
        System.out.println(food);

        Cursor cursor = database.query(MySQLiteHelper.TABLE_FOODS,allColumns,
                MySQLiteHelper.COLUMN_NAME + " = '"+ food+ "' ", null, null, null, null);
        cursor.moveToFirst();
        System.out.println("Food Inserted!!!!!");
        return cursorToFood( cursor);

    }

    public MealCombo createCombo(String listOfFoodItems, Float price){

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_LIST_OF_FOOD, listOfFoodItems);
        values.put(MySQLiteHelper.COLUMN_PRICE, price);
        long insertId = database.insert(MySQLiteHelper.TABLE_COMBOS, null, values);
        System.out.println(listOfFoodItems);

        Cursor cursor = database.query(MySQLiteHelper.TABLE_COMBOS,allColumns2,
                MySQLiteHelper.COLUMN_ID + " = "+ insertId, null, null, null, null);
        cursor.moveToFirst();
        System.out.println("Combo Inserted!!!!!");
        return cursorToCombo( cursor);

    }


    public void deleteTable(){
        database.execSQL("delete from "+ MySQLiteHelper.TABLE_FOODS);
        database.execSQL("delete from "+ MySQLiteHelper.TABLE_COMBOS);

    }

    private Food cursorToFood(Cursor cursor){

        Food food = new Food();
        food.setName(cursor.getString(0));
        food.setPrice(cursor.getFloat(1));
        return food;
    }

    private MealCombo cursorToCombo(Cursor cursor){

        MealCombo combo = new MealCombo();
        combo.setId(Integer.parseInt(cursor.getString(0)));
        combo.setListOfFoodItems(cursor.getString(1));
        combo.setPrice(cursor.getFloat(2));
        return combo;
    }

    public List<Food> getAllFoods(){

        List<Food> foods = new ArrayList<Food>();
        Cursor cursor = database.query( MySQLiteHelper.TABLE_FOODS,
                allColumns, null, null, null, null, null );
                cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            Food food = cursorToFood(cursor);
            foods.add(food);
            cursor.moveToNext();

        }
        return foods;


    }

    public List<MealCombo> getAllCombos(){

        List<MealCombo> combos = new ArrayList<MealCombo>();
        Cursor cursor = database.query( MySQLiteHelper.TABLE_COMBOS,
                allColumns2, null, null, null, null, null );
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            MealCombo combo = cursorToCombo(cursor);
            combos.add(combo);
            cursor.moveToNext();

        }
        return combos;


    }





}
