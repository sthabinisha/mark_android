package com.und.markexercise3;

public class FoodModel {
    FoodModel(){

    }
    public String name;
    boolean checked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getSelected() {
        return checked;
    }

    public void setSelected(boolean checked) {
        this.checked = checked;
    }



    FoodModel(String name, boolean checked){
        this.name = name;
        this.checked = checked;
    }


}
