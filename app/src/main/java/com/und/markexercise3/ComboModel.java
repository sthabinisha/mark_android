package com.und.markexercise3;

public class ComboModel {


    public String id;
    boolean checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getSelected() {
        return checked;
    }

    public void setSelected(boolean checked) {
        this.checked = checked;
    }


    ComboModel(String id, boolean checked){
        this.id = id;
        this.checked = checked;
    }


}
