package com.und.markexercise3;

public class FoodModelPrice {

    FoodModelPrice(){

    }
    public String name;
    public String price;
    boolean checked;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean getSelected() {
        return checked;
    }

    public void setSelected(boolean checked) {
        this.checked = checked;
    }



    FoodModelPrice(String name, String price, boolean checked){
        this.name = name;
        this.price = price;
        this.checked = checked;
    }

}
