package com.und.markexercise3;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import android.widget.RadioGroup;


public class CustomAdapterCheck extends BaseAdapter {

    private Context context;
    public static ArrayList<FoodModelPrice> modelArrayList;

    public CustomAdapterCheck(Context context, ArrayList<FoodModelPrice> modelArrayList){
        this.context = context;
        this.modelArrayList = modelArrayList;
    }

    @Override
    public int getViewTypeCount(){
        return 1;
    }
    @Override
    public int getItemViewType(int position){
        return position;
    }

    @Override
    public int getCount(){
        return (modelArrayList ==null)?0: modelArrayList.size();
    }

    @Override
    public Object getItem(int position){
        return modelArrayList.get(position);
    }
    @Override
    public long getItemId(int position){
        return modelArrayList.size();
    }


    @Override
    public View getView(int postion, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if(convertView == null){

            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row_check,null,true);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.cb2);
            holder.tvFood = (TextView) convertView.findViewById(R.id.food2);
            holder.edPriceQ= (EditText) convertView.findViewById(R.id.pricePurchase);

            convertView.setTag(holder);
        }else
        {
            holder = (ViewHolder)convertView.getTag();
        }

        //holder.checkBox.setText("Checkbox "+ postion);
        holder.tvFood.setText(modelArrayList.get(postion).getName());
        holder.edPriceQ.setText(modelArrayList.get(postion).getPrice());
        modelArrayList.get(postion).setPrice(holder.edPriceQ.getText().toString());

        holder.checkBox.setChecked(modelArrayList.get(postion).getSelected());

        holder.edPriceQ.setTag(R.integer.btnplusview, convertView);
        holder.edPriceQ.setTag(postion);

        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(postion);
//     holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
////         @Override
//////         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//////             if(holder.checkBox.isChecked()){
//////                 modelArrayList.get(buttonView.getId()).setSelected(true);
//////             }
//////
//////         }
//     });
        holder.checkBox.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                View tempview = (View) holder.checkBox.getTag(R.integer.btnplusview);
                TextView tv = (TextView) tempview.findViewById(R.id.food2);
                Integer pos = (Integer) holder.checkBox.getTag();
                //Toast.makeText(context,"Checkbox "+pos+" clicked!", Toast.LENGTH_SHORT).show();

                if(modelArrayList.get(pos).getSelected()) {
                    modelArrayList.get(pos).setSelected(false);
                    Toast.makeText(context,"Checkbox "+pos+" clicked! " +modelArrayList.get(pos).getSelected(), Toast.LENGTH_SHORT).show();




                }else {
                    modelArrayList.get(pos).setSelected(true);
                    Toast.makeText(context,"Checkbox "+pos+" clicked! " +modelArrayList.get(pos).getSelected(), Toast.LENGTH_SHORT).show();


                }
            }

        });


        return convertView;
    }


    private class   ViewHolder{
        protected CheckBox checkBox;
        private TextView tvFood;
        private EditText edPriceQ;
    }



}


