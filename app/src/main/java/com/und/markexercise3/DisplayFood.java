package com.und.markexercise3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DisplayFood extends AppCompatActivity {


    private FoodsDataSource datasource;
    private ListView lv;
    private ArrayList<FoodModelPrice> modelArrayList = new ArrayList<>();
    private ArrayList<FoodModelPrice> modelArrayList2 = new ArrayList<>() ;
    private CustomAdapterCheck customAdapter;
    private List<Food> values;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_display_food);


        lv = (ListView)findViewById(R.id.lvFood);

        datasource = new FoodsDataSource(this);


        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //datasource.deleteTable();

        values = datasource.getAllFoods();
        for (int i = 0; i< values.size(); i++){
            FoodModelPrice model = new FoodModelPrice( values.get(i).getName(),values.get(i).getPrice().toString(), false );
            modelArrayList.add(model);
        }

        customAdapter = new CustomAdapterCheck(this,modelArrayList);
        lv.setAdapter(customAdapter);




    }

    public void onClick(View view){






    }

    public void onClick2(View view){
        EditText et;

        modelArrayList2 = new ArrayList<>();

        for(int i =0; i< modelArrayList.size();i++){
            if(modelArrayList.get(i).getSelected()){
                modelArrayList2.add(modelArrayList.get(i));
            }
        }

        for(Integer i =0; i< modelArrayList2.size();i++){


            Toast.makeText(this, modelArrayList2.get(i).getName() + modelArrayList2.get(i).getPrice(), Toast.LENGTH_SHORT).show();


        }

        //Toast.makeText(context,"Checkbox "+pos+" clicked! " +modelArrayList.get(pos).getSelected(), Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume(){
        try {
            datasource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }




}
